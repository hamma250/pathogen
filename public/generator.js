function randomLink(type) {
   var links;   
   
   if(type == 1){
      // link array
      links = [
         // 1 Krankheitsursachen
         "FkmRTIE0s1w=#",
         "rKxQ0vz1VQg=#",
         "h6%2fI6suYL2c=#",
         "aNBhFpACC98=#",
         "50ZC%2feJpK7U=#",
         // 2 Atrophie, Hypertrophie, Hyperplasie
         "u40%2feWAXtho=#",
         "wCgNBL43izU=#",
         "fpsn49yit4w=#",
         "6ne%2fRGbrnSw=#",
         "RTzEOQR2rlQ=#",
         // 3 Nekrosen
         "2HppoHSk3zA=#",
         "IUmFi3KJbFg=#",
         "fzo22jhoT7o=#",
         "y2o0Y9BI8no=#",
         "HjelkeFSxlY=#",
         // 4 KreislaufstÃ¶rungen
         "4UYValSsJZk=#",
         "xMGEku%2fEK%2bU=#",
         "79huNGBvv%2bk=#",
         "4kcidiuAlZM=#",
         "h2O%2fwtHNhN4=#",
         //5 GefÃ¤sse und Herzinfarkt
         "0suzx%2fIKcCg=#",
         "DY2qPI2Jry8=#",
         "2M8%2bj7UYEds=#",
         "JhO2OmT2%2fgE=#",
         "GlxI5qJ4fsI=#",
         //6 Aktue und chronische EntzÃ¼ndungen
         "feYN7AN3Bys=#",
         "6s1HMZXkGdI=#",
         "ZmvGuvmhI2g=#",
         "aoj1Jgn4H5Q=#",
         "6yn69mQJ4GU=#",
         //7 Immunpathologie
         "mqvoNV9Mxm4=#",
         "l0krv3H9e40=#",
         "p77PkuLzj3A=#",
         "zQPlD9BxJxg=#",
         "HNtaeJ3wY9c=#",
         //8 Pathologische Regeneratio
         "MK7wZgWJHCY=#",
         "aLnZ3yI8Fzk=#",
         "QfgxE%2f6JtLY=#",
         "cGtIBVCoIkg=#",
         "JKaSphqT2dA=#",
         //9 Epitheliale Tumoren
         "KOrblt%2f71E0=#",
         "VL3oi3sCNoU=#",
         "%2btgH%2f1r%2b%2bic=#",
         "0Tx5Ja2srZw=#",
         "8Y%2f3aq6bdqk=#",
         //10 Nichtepitheliale Tumoren
         "vlp1WoudJH4=#",
         "3OyI8gcisFM=#",
         "ps7WiLK0m4Y=#",
         "AV9fXbjcjFE=#",
         "U4MgcLj4GVQ=#",
      ];
   }else{
      // link array path 2
      links = [
         // 1 Krankheitsursachen
         "mp%2bwMWQ6E4I=#",
         "pnZ4pWPc3Ho=#",
         "GTvyG8UP1oU=#",
         "un1ckilpcTc=#",
         "JVeVY4FsZBQ=#",
         "tjRRzfCvMkA=#",
         "mnimGV%2b26dE=#",
         "1Q2agFgKQ6s=#",
         "omOTVj%2fvbb4=#",
         "uW7tYfMNpzw=#",
         "Bjbndk5pD1Y=#",
         "RxURMP44Grc=#",
         "khgTWjBbV6g=#",
         "uj6ZCEthezA=#",
         "s%2fsoq6YXTfU=#",
         "QFlyPzhdU5o=#",
         "cLpj9N4uj3k=#",
         "chDTGZLeWH8=#",
         "EakdoC951c4=#",
         "hUXnzHdtAhY=#",
         "nyBKMHXMo68=#",
         "3h%2fRAAU1ssE=#",
         "b4eUIFZlU4I=#",
         "ujVJm%2bVjTb8=#",
         "yCh%2bzAKGOBs=#",
         "zeIAmTl3JMQ=#",
         "t9j89b9PWPc=#",
         "9vZ%2ftocTGIk=#",
         "iHOTqPc%2b2zc=#",
         "cRS%2bkO1e5YU=#",
         "GZMZLPkN%2b3w=#",
         "5CplthQKcPI=#",
         "Eu7sRk1Wx90=#",
         "W%2bAiR2TEAPg=#",
         "BQpLDYcFQEY=#",
         "HC0bv8tJieQ=#",
         "A8KckCAdgM8=#",
         "xjRI%2bVtnLq0=#",
         "xV4bO77%2fgSg=#",
         "Y%2foo%2fnjBTSs=#",
      ];
   }

	var randomNumber = Math.floor(Math.random() * links.length);

	var link = links[randomNumber];

	return "http://139.30.176.133/MDSD/home.aspx?u=students&sid=" + link;
}

// neuen Tab öffnen
function OpenInNewTab(url) {
	var win = window.open(url);
	win.focus();
}
